package main

import(
	"net/http"
	"log"
	"encoding/json"
	"os"
	"io/ioutil"
)

func PongHandler(w http.ResponseWriter, r *http.Request) {
	host := os.Getenv("SERVER_ONE_HOST")
	resp, err := http.Get("http://" + host + "/ping")
	if err != nil {
		log.Printf(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(map[string]string{"name": os.Getenv("POD_NAME"), 
	"server_1_res": string(body)})
}

func main()  {
	http.HandleFunc("/ping", PongHandler)
	log.Print(http.ListenAndServe(":8080", nil))
}
