package main

import(
	"net/http"
	"log"
	"encoding/json"
	"os"
)

func PongHandler(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(map[string]string{"name": os.Getenv("POD_NAME")})
}

func main()  {
	http.HandleFunc("/ping", PongHandler)
	log.Print(http.ListenAndServe(":8080", nil))
}
